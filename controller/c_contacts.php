<?php

/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 17.05.2016
 * Time: 14:50
 */
class C_Contacts extends Model
{
  function __construct()
  {
    $this->model = new M_About();
    $this->view = new View();
  }
  
  function action_index(){
    $data = $this->model->get_data();
    $this->view->generate('v_about.php','template_view.php',$data );
  }
}