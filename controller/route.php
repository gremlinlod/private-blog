<?php

/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 16.05.2016
 * Time: 15:22
 */
class Route
{
  static function start()
  {
    $controller_name = 'Main';
    $action_name = 'index';
    $routes = explode('/', $_SERVER['REQUEST_URI']);
    if (!empty($routes[1])) {
      $controller_name = $routes[1];
    }

    if (!empty($routes[2])) {
      $action_name = $routes[2];
    }

    //префиксы
    $model_name = 'M_' . $controller_name;
    $controller_name = 'C_' . $controller_name;
    $action_name = 'action_' . $action_name;

    //подцепляем модельку
    $model_file = strtolower($model_name) . '.php';
    $model_path = "model/" . $model_file;
    if (file_exists($model_path)) {
      include "model/" . $model_file;
    }

    //подцепляем контроллер
    $controller_file = strtolower($controller_name) . '.php';
    $controller_path = "controller/" . $controller_file;
    if (file_exists($controller_path)) {
      include "controller/" . $controller_file;
    } else {
      //todo добавить исключение об ошибке
      echo $controller_path;
      die();
      //Route::ErrorPage404();
    }

    $controller = new $controller_name;
    $action = $action_name;

    if (method_exists($controller, $action)) {
      //действие из контроллера
      $controller->$action();
    } else {
      Route::ErrorPage404();
    }
  }

  function ErrorPage404()
  {
    echo $controller_name;
    echo $action_name;
//    $host = 'http://' . $_SERVER['HTTP_HOST'] . '/';
//    header('HTTP/1.1 404 Not Found');
//    header('Status: 404 Not Found');
//    header('Location:' . $host . '404');
  }
}