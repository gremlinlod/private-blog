<?php
/**
 * Created by PhpStorm.
 * User: gremlinlod
 * Date: 01.03.16
 * Time: 0:31
 */

class Model
{
  public function get_data(){

  }
}

function get_articles($link)
{
  $query = "SELECT * FROM `articles` WHERE 1";
  $result = mysqli_query($link, $query);
  if (!$link) {
    die(mysqli_error($link));
  }
  $nrows = mysqli_num_rows($result);
  $articles = array();

  for ($i = 0; $i < $nrows; $i++) {
    $row = mysqli_fetch_assoc($result);
    $articles[] = $row;
  }

  return $articles;
}

//article
function get_article($index)
{
  global $link;
  $id = (int)$index;
  $query = "SELECT * FROM `articles` WHERE `id` = '$id'";
  $result = mysqli_query($link, $query);
  $article = mysqli_fetch_assoc($result);
  if (!$article) {
    //todo add 404 redirect
    die('article not found');
  }

  return $article;
}


function add_article($link, $title, $content)
{
  $title = trim($title);
  $content = trim($content);
  $title = mysqli_real_escape_string($link, $title);
  $content = mysqli_real_escape_string($link, $content);
  $query = "INSERT INTO `articles` (`title`, `content`) VALUES ('$title', '$content')";

  $result = mysqli_query($link, $query);
  if ($result) {
    $id = mysqli_insert_id($link);
    return $id;
  } else {
    echo "error while adding" . "<br>";
    die(mysqli_error($link));
  }
}

function edit_article($link, $index, $title, $content)
{
  $id = (int)$index;
  $title = trim($title);
  $content = trim($content);
  $title = mysqli_real_escape_string($link, $title);
  $content = mysqli_real_escape_string($link, $content);
  $query = "UPDATE `articles` SET `title`='$title',`content`='$content' WHERE `id`='$id'";
  $result = mysqli_query($link, $query);

  echo $index . "<br>" . $title . "<br>" . $content . "<br>";
  if ($result) {
    return 1;
  } else {
    echo "error while editing" . "<br>";
    die(mysqli_error($link));
  }
}

function delete_article($link, $index)
{
  $id = (int)$index;
  $query = "DELETE FROM `articles` WHERE `id` = '$id'";
  $result = mysqli_query($link, $query);

  if ($result) {
    return 1;
  } else {
    die(mysqli_error($link));
  }
}

