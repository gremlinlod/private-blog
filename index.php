<?php
/**
 * Created by PhpStorm.
 * User: gremlinlod
 * Date: 01.03.16
 * Time: 0:06
 */

require_once 'model/model.php';
require_once 'view/view.php';
require_once 'controller/controller.php';
require_once 'controller/route.php';

Route::start();
